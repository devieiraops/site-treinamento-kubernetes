---
id: logging_monitoring
title: Monitoring e Logging
---

### MONITORING

![logging_monitoring](/../img/logging_monitoring.png)
Fonte: https://banzaicloud.com/blog/k8s-logging/

  
---

### MONITORING

É possível realizar monitoramento a nível de Workers.
É possível monitorar recursos como utlização de CPU, Memória, disco e rede em Workers.

Também podemos coletar métricas dos PODS implantandos no cluster.

Existe várias soluções para realizar coleta de métricas em um cluster de Kubernetes.
Opensource temos as seguintes: Metrics-Server, Prometheus, Elastic Stak...
Temos outras proprietárias como: DataDog, Daynatrace.

O Metrics-server armazena os dados coletados somente em memória.

As métricas são geradas por um sub-componente do Kubelet, cAdvisor.
cAdvisor é o componenete responsável por recuperar as métricas dos pods e Workers.
Expondo por meio da api Kubelet para o Metrics-Server.

### LOGGING

Assim como é gerado os logs de um container no Docker, também é gerado no Kubernetes.
Podemos utlizar o comando para obter na saída do terminal os logs dos PODS. 

``kubectl logs (nome do pod)``

Se houver mais de um container no pod é necesário espeficidar o nome do container.

Na documentação temos mais dicas e formas de realizar monitoring e logging em Clusters de Kuberntes.
É uma boa documentação, vários exemplos disponíveis e fáceis de se utlizar.