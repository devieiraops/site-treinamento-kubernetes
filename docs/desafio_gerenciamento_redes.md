---
id: desafio_gerenciamento_redes
title: Desafio Gerenciamento de Redes
---

Nosso desafio será executar requisições de redes em pods.
Muito importante todos realizarem esse desafio!
Fiquem a vontatade para pesquisarem na própria documentação do Kubernetes.
Ela é o melhor lugar para buscar conhecimento e se aprofundar...

  https://kubernetes.io/docs/home/

  

---

1. Executar um deploy de nome nginx com a imagem nginx:alpine


2. Listar todos os deploys


3. Armazene o nome do pod do deploy nginx na variável WEB


4. Listar o IP no pod $WEB executando o comando ip a


5. Use o encaminhamento de porta para estender a porta 80 a 8081


6. Verificar o acesso ao pod $WEB no ip 127.0.0.1 através do comando curl


7. Crie um serviço do tipo nodeport para o deploy nginx na porta 80


8. Listar todos os serviços


9. Verificar o acesso ao serviço $nginx no ip 127.0.0.1 através do


10. Remova o serviço nginx


11. Remova o deploy nginx