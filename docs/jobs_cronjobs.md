---
id: jobs_cronjobs
title: Jobs e CronJobs
---

![jobs](../img/jobs.png)
Fonte: https://frankie-yanfeng.github.io/2019/07/14/Kubernetes_Basics_4-2019/

---

### JOBS    

Os Jobs executam um número especificado de pods e os reinicia continuamente.
Utilizados para tarefas em processamento, fila de tarefas.

Há dois campos que controla a execução dos Jobs: **completions e parallelism.**
- Completions: determina o número de vezes que o Pod deve executar com sucesso antes que o Job seja considerado completo. (default 1)

- Parallelism: Especifíca quantos Pods devem executar ao mesmo tempo. Somente um pod executa a cada vez, usado para consumir filas (default 1)

O modo mais comum de executar um Job é iniciá-lo periodicamente em um dado horário do dia ou em intervalos.
                
Exemplo de objeto Job:
                
```
apiVersion: batch/v1 
kind: Job 
metadata:
 name: queue-worker
spec: completions: 1
     parallelism: 10 
     template: 
      metadata: 
       name: queue-worker
      spec: 
       containers:
       ...
```

##### Criando um Job:

Acessar a pasta do material desta aula:

``cd /kubernetes/jobs``

Realizando o download de um manifesto para dentro do nosso host em um arquivo yaml.

``curl https://kubernetes.io/examples/controllers/job.yaml > jobs.yaml``

``cat jobs.yaml``

``kubectl create -f jobs.yaml``

``kubectl describe jobs.batch pi``

``kubectl get jobs``

Esse comando mostra os logs de um pod, nesse exemplo o nosso job.

``kubectl get logs (nome do pod)``

``kubectl delete -f jobs.yaml ``

---

### CRONJOBS

![jobs](../img/cronjobs.png)
Fonte: https://medium.com/google-cloud/kubernetes-cron-jobs-455fdc32e81a


##### CRONJOBS

Os CronJobs são úteis para criar tarefas periódicas e recorrentes, como executar backups ou enviar emails.
Os CronJobs fazem exatamente o mesmo que o dameon cron do unix.

Podemos utlizar a execução de um job configurando utlizando os mesmos parametros utlizandos no cron unix.
Em seu manifesto yaml temos .spec.schedule: "* * * * *" exatamente no mesmo formato cron.

Em **.spec. jobTemplate:** definimos o Job a ser executado.
Observação: Todos os horários do CronJob schedule: são baseados no fuso horário do kube-controller-manager.

Dica: Se houver mais de 100 agendamentos perdidos, ele não iniciará o job e registrará o erro. **(Cannot determine if job needs to be started. Too many missed start time (> 100). Set or decrease .spec.startingDeadlineSeconds or check clock skew.)**

Por default o cronjobs permite a execução simultânea de trabalhos.
                
Exemplo de objeto CronJobs:

```
apiVersion: batch/v1beta1
kind: CronJob
metadata: 
  name: demo-cron 
spec: 
  schedule: "*/1 * * * *" 
  jobTemplate: spec: 
  ...
```

##### Criando um CronJobs:

Essa é uma forma de obter manifestos da internet para seu localhost.

``curl https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/application/job/cronjob.yaml > cronjobs.yaml``

``cat cronjobs.yaml``

``kubectl create -f cronjobs.yaml``

``kubectl describe cronjobs.batch``

Verificando os logs do coronjob

``kubectl logs (nome do cronjob)``

``kubectl get pods``

``kubectl delete -f cronjobs.yaml ``

##### Teste de conhecimento Jobs e Cronjobs https://forms.gle/XTivJHLpQytbSW1YA

---