openssl genrsa -out bi.key 2048
openssl req -new -key bi.key -out bi.csr

cat <<EOF | kubectl apply -f -
apiVersion: certificates.k8s.io/v1beta1
kind: CertificateSigningRequest
metadata:
  name: bi
spec:
  groups:
  - system:authenticated
  request: $(cat bi.csr | base64 | tr -d "\n")
  usages:
  - client auth
EOF


kubectl get csr
kubectl certificate approve bi
kubectl get csr
kubectl get csr bi -o jsonpath='{.status.certificate}' | base64 -d > bi.crt
kubectl create role bi --verb=create --verb=get --verb=list --verb=update --verb=delete --resource=pods --dry-run -o yaml > rolebi.yaml
kubectl apply -f rolebi.yaml
kubectl get roles.rbac.authorization.k8s.io
kubectl create rolebinding bi-binding --role=bi --user=bi --dry-run -o yaml > rolebindingbi.yaml
kubectl apply -f rolebindingbi.yaml
kubectl get rolebindings.rbac.authorization.k8s.io
kubectl --kubeconfig=bi-kubeconfig config set-credentials bi --client-key=bi.key --client-certificate=bi.crt --embed-certs=true
kubectl --kubeconfig=bi-kubeconfig2 config set-cluster eks-itsm-dev-001 --server=https://172.16.100.217/k8s/clusters/c-d7m67 --certificate-authority=LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUM3akNDQWRhZ0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREFvTVJJd0VBWURWUVFLRXdsMGFHVXQKY21GdVkyZ3hFakFRQmdOVkJBTVRDV05oZEhSc1pTMWpZVEFlRncweU1EQXlNRFF4TWpVMU1qbGFGdzB6TURBeQpNREV4TWpVMU1qbGFNQ2d4RWpBUUJnTlZCQW9UQ1hSb1pTMXlZVzVqYURFU01CQUdBMVVFQXhNSlkyRjBkR3hsCkxXTmhNSUlCSWpBTkJna3Foa2lHOXcwQkFRRUZBQU9DQVE4QU1JSUJDZ0tDQVFFQXlZK2piQy9uZUlPai9JY2gKWTVySVdjSm5oK3dORSthNGFmSGRyV3BwUVIrRjdCc2FUWlJpdy9YR1IrT0ZaZWl5dUtqaVJ4a3llY0s1aHh4dQp3UjRCOXBqbnZzeFVKYXdZTXhteVhxcWh3dkJPNkRCT2lQdi93OWhYRklocjZVZnlHeWJYaitmSmVyYmFXQ1JkCkVWaFhHMUtEY2luMW9qTWdXOG1GelUyOEo5VG9KVmdhaGRCK0hJVlVlSkJmK0JYaGdZdG9hSmtBbklNMUFJZmQKRmZhRkQwYzY1bmwwZ3VTWnplU3k2OUtaQVAwb2JiN3d1bXRIeHZlb21qM1pub3piMmZ0N2JtY21ibHBMSE1JYgpzQ3JzK2YvdDY3UnR6TUFvN1V2NDJFN1V3N3YzMHJqc01FWGo5aFU4K2Z6YjQxUmNtUmNreFZmTVFGVEt0bTVtCm1wNUFyd0lEQVFBQm95TXdJVEFPQmdOVkhROEJBZjhFQkFNQ0FxUXdEd1lEVlIwVEFRSC9CQVV3QXdFQi96QU4KQmdrcWhraUc5dzBCQVFzRkFBT0NBUUVBc0FteGpmbkxzUnd0OEd1RmN1QWltYmtEKzRoUTF3ZXFEOFhBeEUzbgpPQ3ZuRlNtVEdEQ25sM1FYTHRJOENOc01ISStDcE1Qd1o1Vy9SS0g5bklhQmlYN0NsUFc0QVA1cDFGZlBHUVBzCitHVGF2OXZFdjBQbUxBUlZIYnRDeXpPL1B2OTdYZUFoUDFPUklBNjBxTVlwWTJOWDJ0eGthRnoxNElNT0FJTEgKbE5CbUk4RVZXZDJWV2pycWVSUTJBRkZWUHQ2aytuMkkva2E3UnphbmdHdW9tMTZlT2R6RHdhN05DYm0zODA2UwpWWVdPVlhlQ2RmUkVLQ3M5a0d3TWsydXVEU0R4TVd5VmlObi9wRCtENVZ6YmQvc3lHNzRueXlPSlFsUC9PbXJtCkViNXlUb1hPbmtNRzM1aGRHS0JxMEs0ZFRtenBBVkF6eTVwNTFrR0ltaUU1TEE9PQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0t
kubectl --kubeconfig=bi-kubeconfig2 config set-context bi --user=bi --cluster=eks-itsm-dev-001 --namespace=project-compras-develop
kubectl --kubeconfig=bi-kubeconfig2 config use-context bi
kubectl --kubeconfig=bi-kubeconfig2 get pods
kubectl apply -f rolebi.yaml
kubectl --kubeconfig=bi-config port-forward nginx --address localhost 8080:80
