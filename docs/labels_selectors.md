---
id: labels_selectors
title: Labels e Selectors
---


### LABELS E SELECTORS

![labels_selectors](/../img/labels_selectors..png)
https://kubernetes.io/docs/tutorials/kubernetes-basics/expose/expose-intro/


---


### LABELS E SELECTORS            
    

São pares de chaves e valor anexados aos objetos cirados no Kubernetes, por exemplos pods. 
Devem ser utlizdos para identificação dos objetos com isso podemos gerenciar e econtrar objetos criados com maior facilidade.

Podemos defnir um label para pods de production e development entre várias outras, organizando assim nossas implatanções.
Os prefixos **kubernetes.io/e k8s.io/** são reservados para os principais componentes do Kubernetes.

Os valores válidos dos rótulos devem ter 63 caracteres ou menos e devem estar vazios ou começar e terminar com um caractere alfanumérico.
Os requisitos de igualdade ou de desigualdade permitem a filtragem por valores e chaves de labels.

##### O exemplo abaixo possuis duas labels definidas:

```

apiVersion: v1
kind: Pod
metadata:
name: nginx
labels:
    environment: production
    app: nginx
spec:
containers:
- name: nginx
    image: nginx:1.14.2
    ports:
    - containerPort: 80

```

---

Vamos acessar o path que contém os arquivos:

``cd /kubernetes/labels-selectors``

``cat pod.yaml ``

Esse comando busca o pod pelo seu seletor.

``kubectl get pod --selector=app``

``kubectl get pod --selector=environment``

Esse comando busca o pod pela sua label.

``kubectl get pod -l environment=prod,app=nginx``

``kubectl get pod -l environment!=prod,app!=nginx``

``kubectl get pods -l 'environment in (prod),app in (nginx)'``

``kubectl expose pod nginx --port 8080 --target-port 80``

``kubectl describe svc nginx``

Buscando o service pela label

``kubectl get svc -l app``

``kubectl get svc -l environment``

``kubectl delete -f .``

``kubectl delete svc nginx``