---
id: introducao
title: Introdução ao Kubernetes
sidebar_label: Introdução ao Kubernetes
---

### O que é o Kubernetes?

O Kubernetes é uma plataforma portátil, extensível e de código aberto para gerenciar cargas de trabalho e serviços em container, que facilita a configuração declarativa e a automação.
    
Possui um ecossistema grande e de rápido crescimento. Os serviços, suporte e ferramentas do Kubernetes estão amplamente disponíveis.

O nome Kubernetes é originário do grego, significando timoneiro ou piloto. 
O Google abriu o projeto Kubernetes em 2014. 

O Kubernetes combina mais de 15 anos de experiência do Google na execução de cargas de trabalho de produção em grande escala com as melhores idéias e práticas da comunidade.

### O que o Kubernetes não é:

O Kubernetes não é um sistema tradicional PaaS (Platform as a Service) com tudo incluído.
    
Como o Kubernetes opera no nível do container, e não no hardware, ele fornece alguns recursos geralmente aplicáveis às ofertas de PaaS, como implantação, dimensionamento, balanceamento de carga e permite que os usuários integrem suas soluções de registro, monitoramento e alerta. 

No entanto, o Kubernetes não é monolítico e essas soluções padrão são opcionais e conectáveis. 

O Kubernetes fornece os alicerces para a criação de plataformas de desenvolvedores, mas preserva a escolha e a flexibilidade do usuário onde é importante.

Vamos ver como podemos cria um cluster Kubernetes:
    
Você pode implantar um cluster Kubernetes em uma máquina local, nuvem, data center no local ou escolher um cluster Kubernetes gerenciado.
 
Você também pode criar soluções personalizadas em uma ampla variedade de provedores de nuvem ou ambientes bare-metal.

E também, simplesmente você pode criar um cluster Kubernetes em ambientes de aprendizado e desenvolvimento utlizando as ferramentas abaixo:

- Minikube	Docker Desktop	    https://www.docker.com/products/docker-desktop
- kind (Kubernetes IN Docker)	https://kind.sigs.k8s.io/
- Minishift		                https://www.okd.io/minishift/    
- MicroK8s                      https://microk8s.io/
- PWK                           http://play-with-k8s.com/

---

### Instalando Kind :D

A documentação do Kind é muito boa, existes várias possibilidades de configurações.
É possível por exemplo subir um Master e 3 Workers.
Também é possível subir um Cluster High Available, ou seje com 3 Master.
Sugiro que deem uma estudada na documentação e nos exemplos disponíveis para melhor aproventamneto dos recursos do Kind.


https://kind.sigs.k8s.io/docs/user/quick-start
Requisitos:
Você precisa do Docker instalado na sua máquina.
Se você estiver usando Windows pode utlizar o docker desktop ou docker toolbox.


#### Vamos lá:
Esse comando baixa o binário em nosso path local.

``curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.8.1/kind-linux-amd64``

Precisamos dar permissão de execução para o binário.

``chmod +x ./kind``

Vamos mover o binário para o path do filesystem destinado para executavéis.

``mv ./kind /usr/bin/kind``

Pronto a configuração inicial está realizada.
Vamos agora subir o nosso Cluster, executando as definições abaixo:

```

cat <<EOF | kind create cluster --name kind --config=-
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true"
  extraPortMappings:
  - containerPort: 80
    hostPort: 80
    protocol: TCP
  - containerPort: 443
    hostPort: 443
    protocol: TCP
EOF

```

Veja que utlizamos uma configuração personalizada para o Cluster, o Ingress.
Esse é um exemplo de que podemos configurar vários recursos.

Depois de cirar o Cluster vamos validar com o comando:

``kind get clusters``

Agora vamos utlizar o kubectl para interagir direto com a api-server.

``kubectl cluster-info``

Pronto, Estamos pronto para iniciar nosso treinamento totalmente hands on Kubernetes.
<!--Mostar o container do cluster com o docker ps -a -->

