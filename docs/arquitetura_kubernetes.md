---
id: arquitetura_kubernetes
title: Arquitetura Kubernetes
---

![arquitetura_kubernetes](../img/arquitetura_kubernetes.png)
Fonte: https://kubernetes.io/pt/docs/concepts/architecture/cloud-controller/


### Core Concepts: 
Cluster Architecture:
- Kubernetes Architecture <https://kubernetes.io/docs/concepts/overview/components/>

##### Master nomeado como: (Control Plane)

Sua função é planejar, carregar, gerenciar containers nos Worker.
Todos esses componentes são chamados de Control Plane.
   
Seus componentes são: (um mnemônico "CAES" para identificarmos :D)<!--(mnemônico cães)-->

- ETCD banco de dados chave é valor, armazena informações sobre os containers schedulados nos Worker.
Representado pelo CATDOG imagine sempre como chave e valor.
<!--(catdog)--> 

- Kube-Scheduler agenda os containers nos workers.
Representando pelo Mutley sempre fazendo suas loucuras.

<!--(mutley)-->   

- Kube-Controller-Manager (Node-Controller - Replication-Controller) cuida dos Workers, garante o número desejado de containers a ser executados.
Representado pelo labrador, é o guia do componentes.
 <!--(labrador)-->

- Kube-apiserver realiza a comunicação entre os componentes descritos principal componente de gerenciamento do Kubernetes. Comunica-se constantemente com o Kubelet...
Representando pelo Scooby-Doo, sempre desvendando mistérios

Curtiram o mnemônico?   :D
 <!--(Scooby-Doo)-->

![caes](../img/caes.png) 

Containers se comunicam através de DNS interno gerado pelo Kubernetes em container. Precisamos do Docker (Container Runtime Engine) instalado em todo o cluster.. (Masters e Workers)

##### Workers nomeado como: (nodes-nós-minios)

Seus componentes são: (mais um mnemônico (DonKey Kong) :D) 
- Docker - hospedam os containers. Representando por DonKey Kong ele é um container de gigante não é rss...
- Kubelet - é um agente rodando em todo os Workers, ele fica escutando instruções vindas do api-server e implanta e destrói os containers. Representando pela Diddy Kong, no game ela usa seus cabelos longos para levar objetos de um lado para o outro, o Kubelet faz exatamente isso com containers.
- Kube-proxy -  realiza a comunicação entre os containers. Exemplo uma app e um banco de dados rodando em Workers distintos. Representada pela Dixie Kong, no game ela é super rápida dando suas estrelinhas, como kube-proxy levando as requisições entre os Pods.


![donkey%20kong](/../img/donkey%20kong.jpg) <!-- minemonico DonKey Kong-->

Eae? Gostaram dos mnemônicos :D .

É muito importante saber esses componentes, então utilizamos essa estratégia para que saibamos de cabeça quem é quem em um cluster de Kubernetes!

Isso irá facilitar muito o dia dia administrando clusters, saber os componentes onde está cada um deles ajuda muito na hora da adminsitração...

---

# ETCD

É um banco de dados chave e valor distribuído, rápido e seguro, Responde na porta 2379.

Nodes - PODs - Configs - Secrets - Accounts - Roles - Bindings - Others
todas alteração são armazenadas no ETCD.

Duas formas de configurar o ETCD - Kubeadm ( implanta como um POD em Namespaces Kube-system) e binário.

Alta disponibilidade do ETCD múltiplos Masters (números impares - algorítmo)
Certificados tls.

O Kubeadm é uma ferramenta criada para facilitar a implantação de um cluster Kubernetes.

Aqui temos um exemplo instalando o binário...

- Instalando ETCD: (exemplo de instalação por binário) 

 ```
 curl -L https://github.com/etcd-io/etcd/releases/download/v3.3.11/etcd-v3.3.11-linux-amd64.tar.gz -o etcd-v3.3.11-linux-amd64.tar.
 tar xzvf etcd-v3.3.11-linux-amd64.tar.gz
 ./etcd
 ./etcdctl set key1 value1
 ./etcdctl get key1
 ./etcdctl
 ```

---

# Kube-API Server - Arquitetura 


Está no centro das tarefas executadas no cluster.
Responsável por: 
- Autenticar usuário 
- Validar solicitações 
- recuperar e atualizar o ETCD (único componente que interage direto com ETCD) os outros componentes como: Scheduler - Kube-Controller-Manager e Kubelet usam api-server para se atualizarem.

Kubeadm implementa um container em Namespaces kube-system (api-server) no Master

Utilizando api para pegar dados do cluster:

``Kubectl get pods -n kube-system``

Segue o link de um post que achei muito relevante sobre o kube-apiserver:
<https://medium.com/@tsuyoshiushio/kubernetes-in-three-diagrams-6aba8432541c>

---

# Controller Managers ![controller_managers](/../img/controller_managers.png)
Monitora continuamente estados de vários componentes.

Node-Controller: verifica status dos Workers através da api-server.

Verifica a cada 5 segundos a integridade, e garante 40 segundos antes de defini-lo como inacessível. Após 5 minutos de timeout os Pods serão provisionados em um Worker íntegro. (isso pode ser customizável.)

Replication-Controller: Monitora status e garante o número de réplicas desejadas. Se um Pod é terminado, ele cria outro!

Existem ainda vários outros Controllers no Kubernetes:
- Deployment-Controller 
- Namespace-Controller 
- Endpoint-Controller 
- CronJob 
- Job-Controller 
- Pv-Protection-Crontroller 
- Service-Account-Controller 
- Stateful-Set 
- Replicaset 
- Node-Controller 
- PV-Binder-Controller 
- Replication-Controller 
(por default todos são ativados - é possível selecionar quais quer a serem ativados) 

Dica, se algum parar de funcionar olhar aqui: 
##### (Kube-Controller-Manager)
Todos esses componentes são empacotados em um único controller. 

- Instalando Kube-Controller-Manager

``wget https://storage.googleapis.com/kubernetes-release/release/v1.13.0/bin/linux/amd64/kube-controller-manager``

Kubeadm implementa um container em Namespaces kube-system (Kube-Controller-Manager) no Master.

                                              
---

# Kube Scheduler ![kube_scheduler](../img/kube_scheduler.png)
 
 Responsável por agendamento de Pods nos Workers. O Scheduler decide qual Pod continua em qual Worker.
 
 Têm papel importante em distribuir os Pods com integridade nos Workers, ou seja, o Scheduler verifica os recursos solicitados do Pod e aloca em um Worker que tem recursos suficientes para executá-lo.
 
 É possível criar mais de um kube-scheduler e na criação de um pod por exemplo definir qual scheduler irá agendar o pod.
 
 Como é feito:  
   - Em 2 fases: 1 primeiro - filtra os Workers em que não é possível executar o Pod e elimina. 
   - 2 segundo - Rank Workers, atribui uma nota de 0 a 10 a partir dos recursos livres só então aloca o Pod. 
É possível Scheduler por resources: Rquirements and limits - Taints and Tolerantions - Node Selector/Affinity
É possível escrever seu próprio Scheduler.

 - Instalando Kube-scheduler

``wget https://storage.googleapis.com/kubernetes-release/release/v1.13.0/bin/linux/amd64/kube-scheduler``

Kubeadm implementa um container em Namespaces kube-system (Kube-Scheduler-Master)

---

# Kubelet ![kubelet](../img/kubelet.png)
Cria os Pods nos Workers realizando pull das imagens definidas.

Envia relatórios de status dos Workers e containers.

Registra novos Workers no cluster.

Interage deretamente com o kube-apiserver.

- Instalando Kubelet

``wget https://storage.googleapis.com/kubernetes-release/release/v1.13.0/bin/linux/amd64/kubelet ``

``ps -aux | grep kubelet``

Observação: Kubeadm não provisiona um Pods de Kubelet, ele deve ser instalado em todos os Workers.

---

# Kube Proxy ![kubeproxy](../img/kube_proxy.png)
Realiza a comunicação entre os Pods alocados nos Workers, através de uma rede (POD no cluster) virtual que se estende por todos Workers.

Existem vários Pods networks.
  - Calico
  - Weave Net
  - Cilium
  - Flannel

Os ips criados nos Pods são efêmeros... Para resolver esse problema precisamos criar um resource chamado service (componente existente em memória).

O Kube-proxy torna o service acessível por todo o cluster.