---
id: replicasets
title: Replicasets
---

##### REPLICASETS

![replicasets](../img/replicasets.png)
Fonte: https://medium.com/faun/kubernetes-pod-naming-convention-78272fcc53ed


Um Replicaset garante que um número especificado de réplicas de pod esteja em execução.
Ele cumpre sua finalidade criando e excluindo Pods conforme necessário para atingir o número de réplicas solicitado.

Identifica novos Pods a serem adquiridos usando seu seletor.
Se a quantidade de pods for menor do que foi definido no Replicaset um novo pod é criado.

O Deployment contém o recurso de replicaset em sua definição, podemos solicitar a quantidade de replicas em sua criação.
                
##### Criando um Replicaset:

Vamos acessar o path que contém os arquivos:

``cd /kubernetes/replicaset``      

``cat replicaset.yaml``

``kubectl create -f replicaset.yaml``

``kubectl get rs``

``kubectl describe rs/frontend``

Verificando o ciclo de vida de um replicaset

``kubespy status apps/v1 ReplicaSet frontend`` <!--(acompanhar alterações no ReplicaSet) -->

``kubectl delete pod frontend``

``kubectl delete rs frontend``
                
##### ReplicaSet HPA (Horizontal Pod Autoscaler)
Realiza consultas de 30 em 30 segundos baseado no consumo dos pods através de apis 
O componente metrics-server é um pré requisito para realizar as coletas de métricas nos objetos do Kubernetes.
Segue o link do projeto que é open-source.
(metrics-server - https://github.com/kubernetes-sigs/metrics-server).

Vamos acessar a pasta do material desta aula:

``cd /kubernetes/replicaset``

Observação:
##### É preciso definir os requests no deployment para realizar o HPA.

Vamos visualizar o que o metrics-server tem:

``cat metrics-server ``

Criando o recursos do metrics-server

``kubectl create -f metrics-server``

Visualizando os recursos criados

``kubectl get pods -n kube-system``

Validando e verificando métricas coletadas

``kubectl top node``

Visualizando recursos de horizontal pod autoscale.

``cat hpa-nginx.yaml``

``kubect create -f hpa-nginx.yaml``

``kubectl get all -o wide``

``kubectl top pods``                 

``cat hpa-scale-nginx.yaml``

Esse comando cria um recurso de autoscale para o deployment... Definindo o máximo e o mínimo de réplicas e quanto de % CPU será schedulado.

``kubectl autoscale deployment nginx --max=10 --min=5 --cpu-percent=50``

``kubectl create -f hpa-scale-nginx.yaml``

``kubectl get all -o wide``

``kubectl describe hpa nginx``

Visualizando um serviçe para o hpa.

``cat service-hpa-nginx.yaml``

Esse comando realiza a criação de um service para o deployment definindo as portas de acesso.

``kubectl expose deployment nginx --port 80 --target-port 80``

``kubectl get all``

Vamos acessar o container e relizar o stress para ver o hap entrando em ação.

``kubectl run -i --tty stress --image=busybox /bin/sh`` (dentro do container)

Esse comando várias resqusições no service que ciramos, assim realizando o streee que precisamos para ver o hpa em ação.

``while true; do wget -q -O- http://nginx.default.svc.cluster.local; sleep 0.0001; done`` (o Kubernetes criar um dns para todo serviço neste padrão)

Em outro terminal vamos visualizar os pods sendo scalados.

``watch kubectl get all``

Vamos deletar os testes que relizamos para não honerar o cluster.

``kubectl delete -f .``

``kubectl delete pod stress``
                   
##### Teste ReplicaSet https://forms.gle/P8Fu2SNBfP1w6Rbk9

---