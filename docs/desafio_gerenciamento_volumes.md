---
id: desafio_gerenciamento_volumes
title: Desafio Gerenciamento de Volumes
---

Nosso desafio será gerenciar dados em volumes.
Muito importante todos realizarem esse desafio!
Fiquem a vontatade para pesquisarem na própria documentação do Kubernetes.
Ela é o melhor lugar para buscar conhecimento e se aprofundar...

  https://kubernetes.io/docs/home/

  

---

1. Aplicar as configurações do arquivo /kubernetes/persistentvolumes/podnginx.yaml:


2. Verificar se o Pod foi iniciado corretamente:


3. Armazene o nome do Pod na variável $WEB


4. Verifique o motivo do Pod $WEB não estar sendo schedulado.


5. Armazenar o IP do pod WEB na variável IPWEB:


6. Crie um PVC para o Pod.


7. Testar o acesso ao pod utilizando a variável $IPWEB através do comando


8. Crie o arquivo index.html com a frase 'Desafio Kubernetes':


9. Testar o acesso ao pod utilizando a variável $IPWEB através do comando


10. Remova o Deploy através do arquivo /kubernetes/persistentvolumes/podnginx.yaml