---
id: namespaces
title: Namespaces
---

### NAMESPACES ISOLANDO MICROSERVICES

![namespaces](/../img/namespaces.png)
Fonte: google.com/url?sa=i&url=https%3A%2F%2Fwww.mundodocker.com.br%2Fkubernetes-namespaces%2F&psig=AOvVaw1rK_hTiNPtKIjNcH5Dpkd2&ust=1595014859115000&source=images&cd=vfe&ved=0CA0QjhxqFwoTCIDWganD0uoCFQAAAAAdAAAAABAD
---

### NAMESPACES - GERENCIANDO RECURSOS

Utilizando Namespaces conseguimos subdividir um cluster em partes distintas.
Podemos ter uma namespaces de prod e outra de test isoladas em um cluster. (com um Service podemos realizar comunicação entre elas.)

Todos os recursos criados em uma namespace epecíficada são visíveis somente nela mesma.
Podemos definir a quantidade de recursos que uma namesapces poderá utliazar como CPU e memória e até mesmo a quantidade de pods.

Separar os microserviços em uma namespaces é uma boa prática a ser utlizada no kubernetes.
Ao criar um recurso do kubentes sem defnir qual namespace o recurso será criado em namespace default.

A namespace kube-system é para os recursos de arquitetura do cluster kubernetes.
Podemos controles de acessos para usuários por Namespaces.

Podemos bloquear o tráfego de rede em uma namespaces através de network policies.
Cuidado: Ao deletar uma namespace todos os recusos alocados nela serão deletados.

---

##### Trabalhando com Namespaces:

Vamos acessar o path que contém os arquivos:

``cd /kubernetes/namespaces``

O comando abaixo mostra as opções que podemos utlizar para utlizar contextos, clusters no Kubernetes.

``kubectl config``

Verificando o contexto atual

``kubectl config current-context``

Verificando o contexto de forma minificada

``kubectl config view --minify``

Esse comando mostra as namespaces no Cluster

``kubectl get namespaces``

``kubectl describe namespaces default``

``kubectl create namespace curso``

Esse comando troca o contexto atual para a outra namespace

``kubectl config set-context --current --namespace=curso``

``kubectl config view --minify``

``kubectl describe namespaces curso``

Criando um deployment e uma namespace específica.

``kubectl create deployment nginx-curso --image=nginx:alpine --namespace curso``

``kubectl get pods``

``kubectl delete -f deploynginxcurso.yaml``

``cat deploynginxcurso.yaml``

``kubectl create -f deploynginxcurso.yaml`` (observação erro de yaml resources)

``kubectl get all``

``kubectl create deployment nginx-default --image=nginx:alpine --namespace default``

``kubectl get all``

Trocando de namespaces

``kubectl config set-context --current --namespace=default``

``kubectl get all``

``kubectl exec -it nginx-xxxx -- /bin/sh``

``cat /etc/hosts``

``kubectl exec -it -n curso nginx-xxxx -- /bin/sh``

``kubectl get pods -o wide -n curso``

``cat /etc/hosts`` (o ip aponta pro nome do pod, kubu-proxy realiza a comunicação)

``ping ip nginx-curso`` (A policy padrão libera todo o tráfego)

``kubectl delete -f deploynginxcurso.yaml``

---

### Resources Quotas

Vamos definir limites de recursos de cpu e memória para namespaces.

``cat resourcequota.yaml ``

``kubectl create -f resourcequota.yaml``

``kubectl config set-context --current --namespace=curso``

Esse comando mostra os resourcesquotas criados no Cluster.

``kubectl get resourcequotas``

``kubectl describe resourcequotas compute-resources``

``cat deployresourcequota.yaml`` (recursos)

``kubectl create -f deployresourcequota.yaml``

``kubectl get resourcequotas compute-resources``

``kubectl describe deployments.apps nginx-curso``

``kubectl edit deployments.apps nginx-curso`` (aumente recursos apenas dos request e salve)

``kubectl get all``

``kubectl describe rs nginx-curso-xxxx`` (Error creating: pods "nginx-curso-5dc84cd64d-cwjhz" is forbidden: exceeded quota)

``kubectl delete -f .``

---

### QoS Class (Qualidade de serviço)

Agendamento e exclusão dos containers.
Os pods nascem e morrem a todo tempo no kubernetes pq? (o kubernetes utliza classes de QoS para os pods).

Limits e request iguais = QoS class: Guaranteed (isso garante que o pod não será marcado para exclusão em caso de ecxeder os limits definidos).
Limits indefinidos e request definidos = QoS class : Burstable (o pod poderá ser excluido a qualquer momento).

Limits e request não definidos = QoS class: Bestffort (prioridade baixa são os primeiros a serem derrubados).

#### QoS Class Guaranteed:

``cat qosguaranteed.yaml``

Criando o deploy Guaranteed.

``kubectl create -f qosguaranteed.yaml``

``kubectl describe pod nginx-guaranteed``

---

#### QoS Class Burstable:

``cat qosburstable.yaml``

Criando o deploy Burstable.

``kubectl create -f qosburstable.yaml``

``kubectl describe pod nginx-burstable``

---

#### QoS Class Bestffort:

``cat qosbestffort.yaml``

Criando o deploy Bestfoort.

``kubectl create -f qosbestffort.yaml``

``kubectl describe pod nginx-bestffort``

``kubectl delete -f .``

---

##### Teste de conhecimento Namespaces - https://forms.gle/qW87AzadzPGju7GB9