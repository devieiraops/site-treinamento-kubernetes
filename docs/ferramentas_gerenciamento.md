---
id: ferramentas_gerenciamento
title: Ferramentas de Gerenciamento para Kubernetes
---


### Ferramentas de Gerenciamento para Kubernetes

---

#### Ferramentas utilizadas para gerencia eficiente de Clusters Kubernetes
    

> bash-completion -

Para instalar o bash-completion é muito simples:

Instale o package bash-completion

``apt-get install bash-completion``

Adicione ao bashrc o comando:

``echo 'source <(kubectl completion bash)' >>~/.bashrc``

Adicione o script no path:

``kubectl completion bash >/etc/bash_completion.d/kubectl``

Faça reload no zsh para as alterações ficarem ativas.

Segue o link direto para documentação onde é ensinado com mais detalhes:

https://kubernetes.io/docs/tasks/tools/install-kubectl/

---


> zsh

Instalando zsh

``apt install zsh``

Agora vamos configurar alguns plugins:

``curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh | sh; zsh``

``git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions``

``git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting``

Vamos acessar o zshrc para adicinar os plugins instalado:

``vim /root/.zshrc`` (adicionar o nome do plugin) zsh-autosuggestions - zsh-syntax-highlighting

Econtre e linha **plugins** e adicione.

```
plugins=(
        git
        zsh-syntax-highlighting
        zsh-autosuggestions
        )
        
```

Auto complete para o zsh

``kubectl completion zsh``
  
Adicionar ao arquivo zshrc o comando abaixo:

``vim /root/.zshrc/``
    ``source <(kubectl completion zsh)``

Faça reload no zsh para as alterações ficarem ativas.


Segue o link do site oficial do zsh.

https://ohmyz.sh/

---

> kube-ps1 - 

O kube-ps1 deixa deixa seu terminal mostrando sempre o cluster e contexto o seu kubeconfig está setado como ativo.

Vamos instalar:

Baixe o repositório:

``git clone https://github.com/jonmosco/kube-ps1.git``

Navege até a pasta onde está o binário e de permissão de execução ao binário.

``cd kube-ps1/``

``chmod 775 kube-ps1.sh``

Vamos mover o binário para o path do filesystem destinado para executavéis.

``mv kube-ps1.sh /usr/bin/``


Agora vamos adicionar ao bashrc a variável PS1 e o source do binário

``source /usr/bin/kube-ps1.sh``
``PS1='[\u@\h \W $(kube_ps1)]\$ '``

Para o zsh basta adicionar ao arquivo zshrc os comandos:

``source /path/to/kube-ps1.sh``
``PROMPT='$(kube_ps1)'$PROMPT``

https://github.com/jonmosco/kube-ps1

---

> Kubeval

Kubeval é uma ferramenta de grande ajuda, ela tem várias configuraões que podem ser utlizadas, uma delas é validação do yaml. Sugir dar uma olhada na documentação.
Ela tem funcionalidades como verificação do seu yaml, se ele está atendendo os padrões de boas práticas.

Vamos instalar o kubeval:

Baixe os arquivos.

``wget https://github.com/instrumenta/kubeval/releases/latest/download/kubeval-linux-amd64.tar.gz``

Descompacte os arquivos baixados e mova o binário para o path de executáveis.

``tar -xf kubeval-linux-amd64.tar.gz``
``sudo cp kubeval /usr/local/bin``

Vamos realizar um teste com um manifesto.yaml

``kubeval hpa-nginx.yaml``

Segue o link da documentação onde existe algumas funcionalidades do kubeval.

https://kubeval.instrumenta.dev/installation/

---

> kubespy


Com o kubespy é possível realizar um trace no cíclo de vida dos pods, deployments, replicasets por exemplo.
Muito bom utilizar o kubespy para entender como o scheduler trabalha enquanto scaleup e scaledown as implatanção.

Vamos instalar.
Baixe o arquivo através do link.

``wget https://github.com/pulumi/kubespy/releases/download/v0.5.1/kubespy-linux-amd64.tar.gz``

Descompacte o arquivo.

``tar -xf kubespy-linux-amd64.tar.gz``

Navege até a pasta onde está o binário e de permissão de execução ao binário.

``cd /releases/kubespy-linux-amd64/``

``chmod +x kubespy``

Mova o binário para o path de executáveis.

``mv kubespy /usr/bin``

Abra dois terminais para ver o kubespy em ação, no terminal 1 execute:

``kubespy trace deploy nginx``

No terminal dois cire o depoyment nginx e veja o que acontece.

``kubectl create deployment nginx --image=nginx:alpine``

Link do projeto
https://github.com/pulumi/kubespy

---

> Leans

O Leans é sem dúvida um dos melhores dashboards que já utlizei para Kubernetes...
Para mais detalhes acesse:

https://k8slens.dev/

---

> Stern
https://github.com/wercker/stern

---

> Popeye
https://github.com/derailed/popeye

---

> kube-shell - ``kube-shell``
https://github.com/cloudnativelabs/kube-shell

---

> kompose - converte arquivos docker compose para manifestos yaml Kubernetes
https://kubernetes.io/docs/tasks/configure-pod-container/translate-compose-kubernetes/

---

> VsCode > plugins Kubernetes e docker
https://code.visualstudio.com/