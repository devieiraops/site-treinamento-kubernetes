---
id: daemonsets
title: Daemonsets e Static PODS
---

### DAEMONSETS

![daemonsets](/../img/daemonset.png)
  
---

### DAEMONSETS

O objeto Daemonsets tem como função executar um pod garantindo que o mesmo será implantando em todos os wokers.
Sempre que um novo Worker é adicionado o Daemonset é executado criando o pod no Worker.

Utlizando para agents de log, monitoring, amarzenamento ou qualquer outra implatanção que deva estar presente em todos Workers.
Exemplos: Kube-proxy, wave-net (rede para kubernetes) prometheus para monitoramento etc...

Seu manifesto yaml é muito semelhante ao Replicasets mudando apenas o Kind para: Deamonset
O Daemonsets utiliza regras de affinity definidas pelo scheduller, com isso ele realiza a distribuição em todos Workers.

Os Daemonsets são schedulados de duas formas: utlizando NodeAffinity ou .spec.nodeName
                
##### Criando um Deamonsets:

Vamos acessar o path que contém os arquivos:

``cd /kubernetes/daemonsets``

``cat daemonset.yaml``

``kubectl create -f daemonset.yaml``

Esse comando visualiza todos os pods em todas as namespaces

``kubectl get pods --all-namespaces``

``kubectl describe pods -n kube-system``

``kubectl describe -n kube-system daemonsets.apps fluentd-elasticsearch ``

``kubectl delete -f daemonset.yaml`` 
                
##### Exemplo NodeAffinity:

```
nodeAffinity:
  requiredDuringSchedulingIgnoredDuringExecution:
    nodeSelectorTerms:
    - matchFields:
      - key: metadata.name
        operator: In
        values:
        - target-host-name
```    

### Static PODS:

Podemos criar um staticpods sem a ncessidade do kube-apiserver.
Criamos os manifestos de staticpods no path /etc/kubernetes/manifests/.

Podemos mudar o path alterando direto nas configurações do kubelet.
Dessa forma os pods serão mantidos ativos.

Em caso de falha no pod o kubelet o reinicia automaticamente.
Qualquer alteração nos manifestos será aplicada pelo kubelet.

Para remover um staticpod devemos remover o manifesto armazenado no path.
Não é possível edita-lo pelo kube-apiserver.

Só é possível criar objetos pods.
O staticpod estrá rodando somente no Worker onde foi configurado seu manisfesto.


---

##### Teste de conhecimento Daemonset https://forms.gle/h3RKSpuXBoEuZuteA