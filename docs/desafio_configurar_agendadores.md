---
id: desafio_configurar_agendadores
title: Configurar Agendadores no Kubernetes
---

Nosso desafio será realizar configurações que ajudam a administrado nossos microserviços.
Muito importante todos realizarem esse desafio!
Fiquem a vontatade para pesquisarem na própria documentação do kubernetes.
Ela é o melhor lugar para buscar conhecimento e se aprofundar...

  https://kubernetes.io/docs/home/

  

---

1. Defina rótulo desafio=kubernetes para o master:


2. Liste os labels do master:


3. Executar um deploy de nome nginx com a imagem nginx:alpine


4. Armazenar o nome do pod do deploy nginx na variavel WEB


5. Criar o arquivo pod-temp.yaml na pasta /tmp com as informações do pod


6. Editar o arquivo /tmp/pod-temp.yaml para adicionar o label


7. Forçar as configurações do arquivo /tmp/pod-temp.yaml:


8. Verificar se o pod esta alocado no master


9. Remova o deploy nginx
