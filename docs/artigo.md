#### Você sabia que o Kubernetes atribui classes de Quality of Service (QoS) nos PODS do Kubernetes?
Hoje vamos abordar esse tema que é muito importante para alta dispobilidade dos microserviços implantandos no Kubernetes.
##### Vamos falar:
O que é Quality of Service (QoS)
Quais o benefícios de se utlizar QoS em implantações relizadas no Kubernetes
E também claro, botar a mão na massa e criar implantações definindo QoS para elas.
*GoGo #kubemaster* :D 

Sabemos que nossas aplicaçõs devem ter alta dispobilidade, scalabilidade, portabilidade e com o Kubernetes isso é totalmente possível.
Hoje nossas aplicação estão sendo executadas a partir de containers. Utlizamos o Kubernetes como o orquestrador destes containers.
O que muitas vezes passar por despercebido é que no momento em que criamos nossas microserviços no Kubernetes não definimos Quality of Service.
Você que já trabalha com Kubernetes já deve ter observado em algum momento algum POD sendo schedulado de forma automatica. Essa é uma das caractericas do Kubernetes.

É como isso acontece? 
Ao criamos um POD uma classe de serviço é definida no momento de criação. Essa classe defini qual o nível de dispobilidade esse POD terá dentro do Cluter.
É com isso, o Scheduler (componente do control plane) faz a leitura da QoS nos PODs existentes no Cluster é define uma ordem de prioridade para remorção de PODs em caso de uma falta de Recursos no Cluster ao tertarmos provisionar novos PODs neste mesmo Cluster. Esse seria um exemplo típo.
Mas e se eu tiver um acoplamento entre meus microserviços, ou seja, um depende do outro e se faz necessário que ele seja altamente disponíveis. E dessa forma eu não queira que os PODs sejam marcados pelo Schedular para serem removidos em momento algum.

Muito bem, isso é possível de ser feito.
Vamos agora falar sobre as classes de QoS
São elas:

- Guaranteed
- Burstable
- Bestffort

![qos](/qos.png)


---

#### QoS class: Guaranteed: 
Essa classe garante que o POD não será marcado para exclusão em caso de exeder os limits definidos ao POD.
Ou seja, mesmo que meu microserviços chegue ao seu limit de memória e CPU o Scheduler não o marcará para ser removido.
Isso torna os PODs altamente disponíveis no Cluters.
Quando utilizar a classe Guaranteed?
Se um grupo de PODS depedenm um do outro para executarem suas tarefas seria um exemplo prático a ser utlizado. Outro seria se o microserviço deve-se ter altadiponibildade.
Existe algum ponto negativo ou ulizar essa classe?
Se pensarmos bem não, já que queremos sempre entregar o melhor para o cliente. O que será necessário é que no momento da criação do POD especifiquemos que ele deve ter os seus resources (recursos) identicos!
Isso acarreta em uma quantidade de CPU e memória fixa para os PODs e com isso acaba gerando um custo maior de recursos computacionas.

---

#### QoS class: Burstable:
Essa classe define que, o POD poderá ser excluído a qualquer momento! Ou seja, se por algum motívo( e acredite eles existem) o Scheduler precisar remover ou marcar algum POD para remorção os PODS que estiverem com a classe *Burstable* poderam ser reagendados a qualquer momento.
Sendo assim é muito importante definir o grau é o nível de disponibilidade para o microserviço.
E como definimos os PODs para que eles tenham essa classe?
Nas definições de recursos dos PODs devemos deixar definidos da seguinte forma:
- Limits indefinidos, ou seja, sem valor algum.
- E os Requests definidos, ou seja, com algum valor de memória ou cpu para o POD.
Dessa forma o nosso microserviço teria a sua classe definida como Burstable.
Lembre-se é importante entender bem o papel desse microserviço antes de realizar uma definição como essa.

---


E por fim sobre as classes temos a:
#### QoS class: Bestffort:
Essa classe é a que deixa os PODs mais instáveis em um Cluster. Ela diz que os PODs que a possuem serão os primeiros a serem excluídos. E como eu defino um POD com essa classe? Basta não definir os Requests e Limits em um POD para que ele receba a classe *Bestfforf*.
Perceba a importancia de definirmos os recursos e limits em nossa implatações e como elas afetam diretamente nossas aplicações. É muito importante saber gerenciar e pensar nessas estratégias e com isso entregar o melhor para o cliente final que estará utlizando o produtos de software.

---

#### Agora vamos por a mão na massa :D

Para ter acessos aos manifestos utlizados você pode realizar o download através do link:
https://gitlab.com/devieiraops/manifestos-treinamento-kubernetes

Vamos criar nosso POD com a calsse *Guaranteed.*

Vamos verificar o conteúdo do manifesto Guaranteed.
>cat manifestos-treinamento-kubernetes/namespaces/qosguaranteed.yaml

```
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: nginx
  name: nginx-guaranteed
spec:
  containers:
  - image: nginx:alpine
    name: nginx
    resources:
      requests:  // perceba que aqui definimos os request e limits com valores iguais
        cpu: "100m"
        memory: "256Mi"
      limits:
        cpu: "100m"
        memory: "256Mi"
  dnsPolicy: ClusterFirst
  restartPolicy: Always
status: {}
```

Vamos criar o POD:
>kubectl create -f qosguaranteed.yaml
pod/nginx-guaranteed created

Vamos realizar o describe desse POD e ver sua classe de QoS
>kubectl describe pod nginx-guaranteed

Procure pela marcação *QoS Class* na saída do describe.

```
...
    Limits:
      cpu:     100m
      memory:  256Mi
    Requests:
      cpu:        100m
      memory:     256Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-wmjl9 (ro)
Conditions:
  Type              Status
  Initialized       True
  Ready             True
  ContainersReady   True
  PodScheduled      True
Volumes:
  default-token-wmjl9:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-wmjl9
    Optional:    false
QoS Class:       Guaranteed // observer aqui a classe que foi atribuida ao POD.
Node-Selectors:  <none>
...
```
É isso ae conseguimos definir a classe Guaranteed com sucesso :D

---

Agora vamos a um outro exemplo, o *Burstable*.
>cat manifestos-treinamento-kubernetes/namespaces/qosburstable.yaml

```
...
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null      
  labels:
    run: nginx
  name: nginx-burstable        
spec:
  containers:
  - image: nginx:alpine        
    name: nginx
    resources:
      requests: // perceba que definimos somente o requests.
        cpu: "100m"
        memory: "256Mi"        
  dnsPolicy: ClusterFirst      
  restartPolicy: Always        
status: {}
...
```

Vamos criar o POD:
>kubectl create -f qosburstable.yaml
pod/nginx-qosburstable created

Vamos realizar o describe desse POD e ver sua classe de QoS
>kubectl describe pod nginx-qosburstable

Procure pela marcação *QoS Class* na saída do describe.

```
...
    Requests:
      cpu:        100m
      memory:     256Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-wmjl9 (ro)
Conditions:
  Type              Status
  Initialized       True
  Ready             True
  ContainersReady   True
  PodScheduled      True
Volumes:
  default-token-wmjl9:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-wmjl9
    Optional:    false
QoS Class:       Burstable // veja aqui a classe que foi atribuida ao POD
Node-Selectors:  <none>
...
```
É isso ae conseguimos definir a classe Burstable com sucesso :D

---

Agora vamos a um outro exemplo, o *Bestffort*.
>cat manifestos-treinamento-kubernetes/namespaces/qosbestffort.yaml

```
apiVersion: v1
kind: Pod
metadata:
  creationTimestamp: null
  labels:
    run: nginx
  name: nginx-bestffort  
spec:
  containers: 
  - image: nginx:alpine  
    name: nginx
    resources: {}  // perceba que não definimos nenhuma quantidade de recursos ou limits para esse pod.      
  dnsPolicy: ClusterFirst
  restartPolicy: Always  
status: {}
```

Vamos criar o POD:
>kubectl create -f qosbestffort.yaml
pod/nginx-qosbestffort created

Vamos realizar o describe desse POD e ver sua classe de QoS
>kubectl describe pod nginx-qosbestffort

Procure pela marcação *QoS Class*  na saída do describe.

```
...
Conditions:
  Type              Status
  Initialized       True
  Ready             True
  ContainersReady   True
  PodScheduled      True
Volumes:
  default-token-wmjl9:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-wmjl9
    Optional:    false
QoS Class:       BestEffort // aqui temos nossa classe atribuida com sucesso
Node-Selectors:  <none>
...
```
É isso ae conseguimos definir a classe BestEffort com sucesso :D

---

Masse né pessoal, é isso que eu queria mostrar pra vocês que gostam de gerenciar Kubernetes. Esses conceitos e técnicas devem ser utlizadas sempre pesando no melhor para o cliente :D independente do pequeno valor a mais que poderá ser gerado na conta no final do mês (pensando na Guarantted)... O que queremos no final é, Que nossos clientes estejam muito muito satisfeitos com o que estamos relacionado aos produtos de software que criamos. Seguindo essas práticas e dicas certamente o nível dos microserviços implantando no Kubernetes ganham um nível diferenciado

Vlw pessoal. :D

att,
André Vieira.