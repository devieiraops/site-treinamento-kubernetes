---
id: desafio_logs_monitoramento_troubleshooting
title: Logs, Monitoramento e Troubleshooting
---

Nosso desafio será sobre Logs, Monitoramento e
Troubleshooting.
Muito importante todos realizarem esse desafio!
Fiquem a vontatade para pesquisarem na própria documentação do Kubernetes.
Ela é o melhor lugar para buscar conhecimento e se aprofundar...

  https://kubernetes.io/docs/home/

  

---


1. Executar um deploy de nome nginx com a imagem nginx:alpine


2. Visualizar os Logs do Deploy nginx


3. Armazenar o nome do pod do deploy nginx na variável WEB


4. Visualizar os Logs do Pod $WEB:


5. Visualizar os Logs nas ultimas 2 horas do Pod $WEB:


6. Visualizar as ultimas 5 linhas do Log do Pod $WEB


7. Visualize as métricas dos Pods em todos os namespaces no Cluster:


8. Visualizar a utilização de CPU, memória e discos por nodes:


9. Remova o deploy nginx