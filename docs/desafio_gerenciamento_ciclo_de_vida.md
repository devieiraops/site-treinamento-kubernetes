---
id: desafio_gerenciamento_ciclo_de_vida
title: Desafio Gerenciar Ciclo de Vida das Aplicações
---

Nosso desafio será baseados em tarefas de gerenciamneto do ciclo de vida das aplicação.
Muito importante todos realizarem esse desafio!
Fiquem a vontatade para pesquisarem na própria documentação do Kubernetes.
Ela é o melhor lugar para buscar conhecimento e se aprofundar...

  https://kubernetes.io/docs/home/

  

---

1. Executar um deploy de nome nginx com a imagem apache2


2. Realizar o escalonamento do Deploy para três replicas:


3. Verificar a quantidade de Pods


4. Redirecionar as informações do Deploy para um novo arquivo YAML /tmp/deploy-nginx.yaml:


5. Editar o arquivo deploy-nginx.yaml, para alterar a quantidade de


6. Alterar a quantidade de replicas do Deploy através do arquivo /tmp/deploy-nginx.yaml:


7. Realizar uma alteração no deploy substituindo a image Docker apache2 para nginx:


8. Verificar o estado do rollback:


9. Desfazer a alteração no Deploy:


10. Exibir históricos das alterações em Deploy:


11. Remova o deploy nginx
