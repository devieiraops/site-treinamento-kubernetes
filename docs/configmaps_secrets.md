---
id: configmaps_secrets
title: Configmaps e Secrets
---

### CONFIGMAPS E SECRETS EM KUBERNETES

Começando por:

### CONFIGMAPS

![configmaps](/../img/configmaps_secrets.png)
  
---

##### CONFIGMAPS
Os ConfigMaps vinculam arquivos de configuração, argumentos de linha de comando, variáveis de ambiente, números de porta e outros artefatos de configuração aos containers e componentes do sistema dos pods no ambiente de execução.

Permitem separar as configurações dos Pods e componentes.
São úteis para armazenar e compartilhar informações de configuração não confidenciais e não criptografadas.

É possível criar configmaps apartir de arquivos ou de forma literal.
É possível adicionar um ConfigMaps a volumes.

##### Utilizando ConfigMpas:

Vamos acessar o path que contém os arquivos:

``cd /kubernetes/configmaps-secrets/``

``cat configmap.yaml``

Esse comando cria um configmap utilizando as configs de um arquivo.

``kubectl create configmap redis-config --from-file=redis-config``

Esse comando lista os configmpas

``kubectl get configmaps``

``kubectl describe configmaps redis-config``

``cat podredis.yaml``

``kubectl create -f podredis.yaml``

``kubectl get pods``

``kubectl describe pod redis``

Acessando o pod

``kubectl exec -it redis redis-cli``

``kubectl exec -it redis redis-cli`` (Dentro do container)
                        
```
127.0.0.1:6379> CONFIG GET maxmemory
1) "maxmemory"
2) "2097152"
127.0.0.1:6379> CONFIG GET maxmemory-policy
1) "maxmemory-policy"
2) "allkeys-lru"

```

``kubectl delete -f .``

``kubectl delete confimaps redis-config``
            
---

### SECRETS

![configmaps](/../img/configmaps_secrets.png)

##### SECRETS
Secrets são objetos seguros que armazenam dados confidenciais, como senhas, tokens OAuth e chaves SSH, nos clusters.
O uso de secrets permite controlar como os dados confidenciais são usados e reduz o risco de expor os dados a usuários não autorizados.

São parecidos com os configmaps, porém conseguem gerar criptografar os valores em base 64 no Kubernetes.
Dessa forma são ideias para valores sensíveis.

As Secrets são criadas e configuradas em um POD.
Podemos criar de forma imperativa ou até mesmo a partir de um arquivo.
É uma boa prática sempre converter os values em base64.

##### Utilizando Secrets:

``cat username.txt``

``cat password.txt``

Criando uma secret de forma imperativa utilizando os arquivos que definem os segredos.

``kubectl create secret generic credentials --from-file ./username.txt --from-file ./password.txt``

``kubectl get secret credentials -o yaml``

Convertendo em base 64

``echo 'YWRtaW4=' | base64 --decode``

``echo 'a3ViZQ==' | base64 --decode``

``kubectl describe secrets credentials``

``cat pod.yaml``

``kubectl create -f pod.yaml``

``kubectl exec -it redis -- /bin/bas`` (dentro do pod)

``env``

``kubectl delete -f pod.yaml``

``kubectl delete secrets credentials``

---

##### Teste de conhecimento Configmpas e Secrets https://forms.gle/XG1aDVE5Ws5QUbpT7
