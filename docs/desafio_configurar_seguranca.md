---
id: desafio_configurar_seguranca
title: Configurar Segurança no Kubernetes
---

Nosso desafio será sobre configuração de segurança em um Cluster.
Muito importante todos realizarem esse desafio!
Fiquem a vontatade para pesquisarem na própria documentação do Kubernetes.
Ela é o melhor lugar para buscar conhecimento e se aprofundar...

  https://kubernetes.io/docs/home/

  

---

1. Listar todos os usuários em todos os namespaces:


2. Listar todos os secrets em todos os namespaces:


3. Listar todas as roles em todos os namespaces:


4. Listar todas as RoleBinding em todos os namespaces:


5. Listar todas as Cluster Role já existem no Cluster:


6. Exibir os detalhes da ClusterRole secret-reader:


7. Listar todas as CluserRoleBinding em todos os namespaces:


8. Listar os usuários nas configurações kubeconfig:


9. Visualizar os contextos do kubeconfig:

