---
id: network_services
title: NETWORK KUBERNETES
---

### NETWORK SERVICES

Começando por:

### SERVICES

![service](/../img/service.png)
Fonte: https://linuxacademy.com/blog/containers/deploying-services-in-kubernetes/
  
---

Os services permitem a conectividade entre um grupo de containers através de uma label que é definida no recurso pods.
Ao utlizar o Service será possível criar uma conexão de rede com o Pod através de Selector.

Mesmo existindo várias réplicas do Pod em questão o service realiza essa conexão de rede com sucesso.
O Service disponibilza um IP único imutável em sua criação é também DNS que pode ser roteado para o Pod com um slector definido.

Trabalha como se fosse um balanceador de cargas ou proxy roteando as requisiÇões vindas em seu IP para os Pods.
Um Service fornece um endpoint imutável para cada grupo de pods através de selector.

Em caso de termos mais de um pod encaminhamento é feito por um algorítimo identificando as labels definidas realizando assim um balaciamento para dentro do pod através do service.

##### Criando um Deployment:

Vamos acessar o path que contém os arquivos:

``cd /Kubernetes/service``

``cat deploynginx.yaml``

``kubectl create -f deploynginx.yaml``

##### Criando um Service (Cluster IP default):

Esse comando cria um service para o deployment nginx definindo um port e um target port para o container.

``kubectl expose deployment nginx --port 80 --target-port 80``

Podemos descrever também os nossos services
``kubectl describe services nginx``

Mostrando o service criado

``kubectl get svc`` (Vamos pegar o IP)

Todo service cria seu endpoint, podemos visualizar com o commando:

``kubectl get endpoints`` (anote o ip - ip do container)

Com esse comando entramos dentro do container.

``kubectl exec -it deploy/nginx -- /bin/sh``

Vamos executar o curl no ip do service

``curl "ip do service"``(dentro do container)    

Vamos executar um ping no ip do service

``ping IP service``

Vamos executar o curl no endpoint

``curl "ip endipoint"``

Vamos executar o pingo no ip do container

``curl "ip do container"``

#### CLUSTERIP

É utlizado no service por default, ou seja, se ciramos um service em definir o seu tipo ele será ClusterIP
Esse tipo é muito ulizado para comunicação dos pods no cluster.

Os ips dos pods não são estáticos. 
O service ClusterIP realiza a interface de comunicação com pods front-end e back-end por exemplo.
Isso tudo de forma flexível, já que ao scalar para mais ou menos réplicas de pods eles perdem seus ips que foram definidos. 
Para executar o serviço usamos as labels.

##### Acessando através do port-forward

Esse comando realiza um encaminhamento de tráfego do seu localhost para dentro do container.

``kubectl port-forward nginx-5b6fb6dd96-gf6f4 --address IPLocalhost 8080:80``

``curl localhost:8080`` (IP do seu localhost)
                
#### NODEPORT
Expoem uma porta para ser acessda, e com isso emcaminhar requsições para o cluster.
Fornece uma porta onde é possível acessar do localhost para a aplicação que está no pod em uma rede isolada, seu intervalo de portas (30000 a 32767).

##### Modificando para type NodePort:
Com esse comando podemos editar um recurso já existente no Kubernetes.
Ao salvar e sair da edição o recurso será implmentando com as novas espcificações definidas.

``kubectl edit service nginx`` (mudar o type para nodePort)

Veja que agora o service tem uma porta.

``kubectl get svc`` (anote a porta NodePort)

``curl IPLocalhost:NodePort``

#### LOAD BALANCER
Expoem um IP exposto para internet e encaminha as requisições para o cluster > service > deployments > pods > containers                

##### Criando Service LoadBalancer: 
Vamos deletar os services criados.

``kubectl delete svc nginx``

Criando um service passando o type no commandline.

``kubectl expose deployment nginx --port 80 --target-port 80 --type LoadBalancer``

``kubectl get svc``

``curl IPLocalhost`` (EXTENAL-IP = pending)    

##### Veja que é criado um Service de cada tipo.

``kubectl get svc``
                    
#### Criando outro Service LoadBalancer:

``kubectl create deployment nginx2 --image=nginx:alpine``

``kubectl expose deployment nginx2 --port 81 --target-port 80 --type LoadBalancer``

``kubectl get svc``

``curl IPLocalhost``        

##### Teste de conhecimento Services https://forms.gle/GfWWhE7ZwFqjYaPa8

---
