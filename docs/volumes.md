---
id: volumes
title: STORAGE EM KUBERNETES
---

### STORAGE EM KUBERNETES

Começando por:

### VOLUMES

![volumes](/../img/volumes.png)
Fonte: https://medium.com/@tharangarajapaksha/kubernetes-volumes-79ff58ec32e8

  
---

Os arquivos gravandos em um container são efêmeros. 
Quando um container falha, o kubelet o reinicia, e os arquivos são perdidos.

Ao executar um ou mais containers em um Pod, geralmente é necessário compartilhar arquivos entre esses containers. 
O Volume do Kubernetes resolve esses dois problemas.

Há vários tipos diferentes de Volumes que podem ser associados a um Pod.
Com isso podemos realizar a persistência dos dados, já que os containers mantêm dados somente equantos estão ativos.

Podemos montar um Volume em pods e compartilhar os dados entre os containers.
Para isso precisamos de um objeto do Kubernetes. O StorageClass com ele podemos criar volumes dinamicamente.

##### De forma prática vamos pensar na seqüência: 

- StorageClass 
- Persistent Volume 
- Persistent Volume Claim 
- Requisição do Pod ao PVC

---

### Storage Class:

Possibilita criar classes de armazenamento definindo políticas de backup, qualidade de serviço determinadas por um adminsitrator.
Cada StorageClass contém o provisionador de campos, parâmetros e reclaimPolicy que são usados quando um PersistentVolume possui sua classe definida.

Observação:
O StorageClass e os objetos não podem ser atualizados depois de criados.
            
##### Criando um StorageClass:

Vamos acessar o path que contém os arquivos:

``cd /kubernetes/persistentvolumes/``

``cat storage.yaml``

``kubectl create -f storage.yaml``

Esse comando mostra todos os storage class do cluster.
Lembrando esse recurso é a nível de cluster e não de namespaces.

``kubectl get sc storage``

``kubectl describe sc sotage``

##### Observação:

**provisioner**: Determina qual plug-in de volume é usado para provisionar Pvs (persistent volume);
**volumeBindingMode**: Permite controlar quando a vinculação de volume e o provisionamento dinâmico devem ocorrer. 

O valor **WaitForFirstConsumer** atrasará a vinculação(**Bound**) e o provisionamento de um PersistentVolume, até que um Pod usando PersistentVolumeClaim seja criado.                

---


## Tipos de Volumes existentes no Kubernetes:



### Volumes emptyDir:

![volume](/../img/volume.png)



Armazenamento efêmero, haverá persistência somente equanto o Pod está sendo executado.
Ideal para cache de arquivos e arquivos temporários.

Observação: O Kubernetes não garante proteção contra locks de escrita em disco, devemos implementar nossos próprios sistemas de locks como nfs ou glusterfs.
            
##### Criando um Volume emptyDir:

Vamos acessar o path que contém os arquivos:

``cd /kubernetes/persistentvolumes``

``cat emptydir.yaml``

Estamos cirando um deployment com emptydir.

``kubectl create -f emptydir.yaml``

``kubectl get pods``

``kubectl describe pods nginx ``

Vamos acessar o container

``kubectl exec -it nginx -- /bin/sh`` (dentro do container)

``cd /usr/share/nginx/``

Esse comando cira um arquivo e escreve dentro dele.

``echo "nginx utilizando volume emptyDir" > index.html``

``cat index.html``

Saindo do container.

``exit``

Esse comando troca a imagem do container.

``kubectl set image pod/nginx nginx=nginx:1.16.1 --record``

``kubectl get pods`` (observe restart)
                
##### Vamos acessar o container novamente:
Etrando dentro do container

``kubectl exec -it nginx -- /bin/sh`` (dentro do container)

Verificando os dados novamente.

``cat /usr/share/nginx/index.html``

Obverve que o dado que alteramos foi mantido.

---

Vamos deletar e seguir para o próximo exemplo.

``kubectl delete -f emptydir.yaml``

---
       
### Persistent Volume:
Para manter dados persistentes devemos usar Persistent Volumes outro recurso do Kubernetes.
Devemos nos preocupar sempre em criar nosso PV e gerenciar os bakcups dos PV's, podemos utlizar um provedor de nuvem para cirar blocos de amazenamentos ou um servidor nfs por exemplo.

Devemos garantir o backups dos dados persistentes.
Há duas maneiras pelas quais os PVs podem ser provisionados: estaticamente ou dinamicamente.

Para utlizar um PV devemos utlizar outro recurso o Persistent Volume Calim.
O PVC faz uma solicitação ao PV definindo quando de amazenamento será ncessário, e outras definições como leitura e escrita.
            
##### Criando um Persistent Volume:

``cat pv.yaml``

Criando um persistent volume.

``kubectl create -f pv.yaml``

Esse comando mostra os persistents volumes criados.

``kubectl get pv``

``kubectl describe pv nginx-pv``
                            
##### Persistent Volume Claim:    
Um PVC é uma solicitação de armazenamento.
Os PVCs consomem recursos dos Persistent Volumes.

Podemos requisitar o tamanho do armazenamento, tipos de acesso como leitura e gravação.
Por default quando um PV é liberado de sua reividiação é **Retain**. Existe outras opções como **delete**, **recycle**.

Ao criar um PVC o control-plane realiza o vínculo com o PV e seu status passa a ser **Bound**.
            
##### Criando um Persistent Volume Claim:

``cat pvc.yaml``

Criando um persistent volume claim.

``kubectl create -f pvc.yaml``

``kubectl get pvc``

Descrevendo o status de bound do pv e pvc.

``kubectl describe pvc nginx-pv-claim``
                
Vamos deletar o PV e PVC.

``kubectl delete -f pv.yaml``

``kubectl delete -f pvc.yaml``

##### Criando Pod com PVC:

Vamos observar que o deployment agora possui novas especificações de volumes.

``cat podnginx.yaml``

``kubectl create -f podnginx.yaml``

``kubectl get pods``

Descrevendo o motivo de falha para schedular o deployment.

``kubectl describe pod nginx`` ( Warning  FailedScheduling  <unknown>  default-scheduler  persistentvolumeclaim "nginx-pv-claim" not found)

``kubectl create -f pvc.yaml``

Agora podemos ver que o deployment foi schedulado com sucesso.

``kubectl get pods``

Esse comando mostra os pvc's criados. 

``kubectl get pvc`` (nginx-pv-claim   Bound    pvc-7d97fff1-75a0-4124-8e86-55ad025fc45d   10Gi       RWO            storage-clas)

``kubectl get pv`` (pv criado dinamicamente)

Se você estiver ussando o Kind como seu cluster de aprendizado ao acessar o container do Kind podemos localizar o pvc criado com sucesso.

``docker ps -a``

Localize o ID do container Kind.

``docker exec -it 6dde14ae0dfa /bin/bash`` (Dentro do container)

Navegue até a pasta:

`` ls /var/local-path-provisioner/``

Veja que a saída mostra o pvc criado com um hash que pode ser idenficado ao realizar o comando kubectl get pv.

``pvc-408e2ee7-fe6c-42e3-a42d-f410fd1a9bc0``

Execute o comando:

``kubectl describe pv (nome do pv criado dinamicamente)``

Veja que em .spec.path o local onde está armazenado os dados persistentes.
Muito importante saber rezalizar a gestão desses dados, seja em um nfs ou cloud provider.
Garantir rotinas de backups, acessos a pessoal autorizado etc..m

Agora vamos entrar em nosso deployment.

``kubectl exec -it nginx -- /bin/sh`` (dentro do container)

``touch /usr/share/nginx/html/index.html``

``cd /usr/share/nginx/html/``

``echo "index.html em volume persistent" > index.html``

``cat /usr/share/nginx/html/index.html``

``exit``

Vamos deletar o deployment e observar que o pv pvc ainda vão estar criados.

``kubectl delete -f podnginx.yaml``

``kubectl get pvc``

``kubectl get pv``

(Delete o PV ao liberar a reivindicação)
Se deletar o pv o pvc ainda continua criado esperando a liberação do pvc, ou seja, que o pvc seja deletado.
Ao deletar o pvc se a política de **Retain** estiver ativada os dados ainda serão mantidos dentro do /var/local-path-provisioner/.
É muito importante saber utlizar as políticas certas, isso pode ajudar na recuperação de dados e também na redução de custos relacionados a discos.

##### Expanding Persistent Volumes Claims:

É possiível realizar expansão dos volumes desde o storage class associado a ele esteja com a política **allowVolumeExpansion** definida como **true**.
Não é possível realizar resizing para um tamanho menor do que foi definido em sua criação, ou seja, se criamos um pvc de 10GB ao tentar realizar o resizing para 5GB
o Kubernetes não atualiza o deployment com essa definição.

Uma mensagem como essa deve aparecer **spec.resources.requests.storage: Forbidden: field can not be less than previous value**.
Pode ser que nas próximas versões do Kubernetes essa opcção esteja disponível.

Vamos aguardar :D

##### Teste de conhecimento Volumes Persistentes https://forms.gle/pGuuhL77anmy7zNs9

---