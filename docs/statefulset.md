---
id: statefulset
title: Statefulset
---

![statefulset](../img/statefulset.png)
Fonte: https://itnext.io/introduction-to-stateful-services-kubernetes-6018fd99338d
---

##### STATEFULSET

Conceito importante de aplicações:

- Stateless (não salva dados de um cliente utlizados em uma sessão, ele não precisa desses dados para o cliente usar na próxima sessão. 
  Aplicação sem estado, não salva dados dos clientes usados na sessão)
                
- Statefull (armazena os dados da sessão do cliente, páginas dinâmicas, cookies etc. 
  Salva dados de sessão do usuário e utliza quando o cliente fizer uma nova solictação)
                
Objeto StateFulSet Kubernetes é ideal para aplicações que desejam manter estado.
                
Cada vez que um pod é reiniciado ele recebe um novo UID e também um novo endereço IP sendo assim não conseguimos manter a sessão do usuário.
                
Com Statefulset é possível criar uma identidade persistente do UID dos pods.
                
Todas as vezes que um pod falhar, reiniciar ele sempre irá retoranr como o mesmo identificador UID, mantendo seu hostname como um indice.
É possível eliminar e atualizar de forma ordenada e automatizada os containers observando o indice.
 
O Statefulset sempre irá precisar de um service agrupado a ele para relizar a comunição do grupo de pods.
 
É também sempre irá precisar de um volume onde serão armazenados os dados das sessões por exemplo.

Os volumes são criados dinamicamentes em cada réplica criada (Local storage não é possível criar volumes dinamicamente)
Deployments são indicados para aplicações sem estado.
                
##### Criando um Service:

Acessar a pasta com o material desta aula:

``cd /kubernetes/statefulset/``

``cat nginx-service.yaml``

``kubectl create -f nginx-service.yaml``

Esse comando mostra todos os services criados.

``kubectl get svc``

##### Criando um Storage-Class:

``cat storage-class.yaml``

``kubectl create -f storage-class.yaml``

Esse comando mostra todos os storage class criados.

``kubectl get sc``

##### Criando um Persistent Volume:

``cat pv.yaml``

``kubectl create -f pv.yaml``

``kubectl delete -f .``  

##### Criando um Statefulset:

``cat statefulset.yaml``

``kubectl create -f statefulset.yaml``

---

##### Teste de conhecimento Statefulset https://forms.gle/EN3VcdzurBmczEzT9