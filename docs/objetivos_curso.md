---
id: objetivos_curso
title: Objetivos do Treinamento
sidebar_label: Objetivos do Treinamento
---


- Neste treinamento você irá aprender a utilizar o Kubernetes e administrar os microservices que iremos implatar. 

- Vamos de forma prática aplicar todo o conteúdo abordado.

- Vamos entender de ponta a ponta o core do Kubernetes criando na prática vários microservices demonstrando como cada componente do kubernetes trabalha.

- Criar clusters de Kubernetes e já sair executando seus Workloads.

- Criar implatanções, services, igress, pods, configmaps, secrets e muito mais...

- Gerenciar o clico de vida das implatanções

- Utilizar Volumes persistentes
  
- Configurar configmaps e secrets para suas implantações
  
- Utilizar recursos do Ingress expondo sua aplicação para foda do Cluster
  
- Coletar métricas dos pods e clusters

- Verificar logs da implantações criadas

- Troubleshooting dos principais erros que aparecen quando criamos implantações


### Após o treinamento você estará apto a:

- Princípios e fundamentos Kubernetes
- Entender a arquitetura Kubernetes
- Entender os componentes de um Master e Worker
- Gerenciar ciclo de vida dos Workloads em Kubernetes
- Utilizar api-server Kubectl
- Criar manifestos yaml
- Gerenciar recursos computacionais em implantações
- Gerenciar dados persistentes.
- Gerenciar Microservices e suas Namespaces
- Expor aplicações para fora do Cluster
- Troubleshooting em Workloads

### Vamos lá :D
