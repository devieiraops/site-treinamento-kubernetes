---
id: deployments
title: Deployments
---

### DEPLOYMENT

![deployments](/../img/depoyment.png)
Fonte: https://medium.com/polarsquad/check-your-kubernetes-deployments-46dbfbc47a7c

Deployment é um objeto do kubernetes capaz de realizar atualizações declarativas pods e criar replicasets.

Podemos definir a quantidade de containers que será implementando, recursos como memoria e cpu, entre outras definições.

Deployment possuem os Replicasets e atualizá-la seus Pods por meio de atualizações progressivas e declarativas.

Garante réplicas de Pods em execução.
É possível realizar rollouts de versões de implantações criadas.

--- 

##### Criando um Deployment

Vamos acessar o path que contém os arquivos:

``cd /kubernetes/deployments``        

``cat nginx-deployment.yaml``

Este comando faz a troca da imagem utlizada no deployment e realiza a gravação.

``kubectl set image deployment.v1.apps/nginx-deployment nginx=nginx:alpine --record=true``

``kubectl describe deploy nginx``
                    
##### Verificando detalhes de revisão:

Esse comando realiza o rollout retornando o deployment para a última alteração realizada no deploy.

``kubectl rollout undo deployment nginx-deployment`` 

``kubectl describe deploy nginx``

Esse comando mostra o manifesto criando em um revisão específica.

``kubectl rollout history deployment.v1.apps/nginx-deployment --revision=2``

Esse comando realiza o rollout para uma revisão específica.

``kubectl rollout undo deployment.v1.apps/nginx-deployment --to-revision=2``

##### Escalando o número de réplicas de um Deployment:

O comando realiza o scale do deployment para uma quantidade de replicas definida.

``kubectl scale deployment.v1.apps/nginx-deployment --replicas=5``

O comando mostra os objetos de replicasets criados.

``kubectl get rs``
                
##### Possíveis mensagens de falhas ao realizar implantações:

- Insufficient quota (limits para alocações de recursos em namespace)
- Readiness probe failures (esperando estado do pod estar em Success)
- Image pull errors (não foi possível realizar carregar a imagem)
- Insufficient permissions (permissão de acesso)
- Limit ranges (limits para alocações de recursos)
- Application runtime misconfiguration (erros de configuração)

##### Editar um POD

Lembre-se de que NÃO É POSSÍVEL editar algumas especificações de um POD existente, a menos que seja as seguintes.

- spec.containers [*]. imagem
- spec.initContainers [*]. imagem
- spec.activeDeadlineSeconds
- tolerâncias
 
##### Editar Deployments

Com as implantações, você pode editar facilmente qualquer campo do POD. 
Como o pod é um filho, ou seja, (está vinculado)  de um deployment, a cada alteração no deployment excluirá automaticamente e criará um novo pod com as novas alterações.       
     
##### Teste de conhecimento deployment https://forms.gle/8aZhwncJHBCP9sWy8