---
id: desafio_gerenciamento_cluster
title: Desafio Gerenciamento do Cluster
---

Nosso desafio será baseado em tarefas simples que executamos no dia dia gerenciando um cluster.
Muito importante todos realizarem esse desafio!
Fiquem a vontatade para pesquisarem na própria documentação do Kubernetes.
Ela é o melhor lugar para buscar conhecimento e se aprofundar...

  https://kubernetes.io/docs/home/

  

---

1. Listar todos os nós no cluster


2. Executar um deploy de nome busybox com a imagem busybox


3. Listar todos os pods em todos os namespaces


4. Listar todos os namespaces no cluster


5. Listar todos os pods no namespace default


6. Listar todos os pods em todos os namespace mostrando o IP


7. Listar todos os deploys


8. Remover um deploy de nome busybox

