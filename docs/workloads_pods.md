---
id: workloads_pods
title: WORKLOADS
---

### WORKLOADS DO KUBERNETES

Começando por:

### PODS 

![pod](/../img/pod.png)
Fonte: https://kubernetes.io/docs/tutorials/kubernetes-basics/explore/explore-intro/

Primeiro vamos realizar o download do repositório com os arquivos que iremos utlizar no treinamneto:

``git clone https://gitlab.com/devieiraops/manifestos-treinamento-kubernetes.git``

Os containers são encapsulados em um objeto Kubernetes conhecido como PODS.
Este é o menor objeto que pode ser criado no Kubernetes.

Podemos ter mais de um continer em um único POD, para realizar tarefas paralelas. 
Eles são se comunicam em localhost, já que estão no mesmo espaço de rede. 
Podem compartilhespaço de amazenamento.

##### Como criar um pod? <!--forma declarativa-->

Criando um deployment

``kubectl run nginx --image=nginx:alpine`` 

O comando acima cria um pod utlizando uma imagem nginx:alpine realizando o download do docker-hub (o resposável por isso é o docker-container-runtime e Kubelet)

O Scheduler entra em ação fazendo seu papel alocando o pod no cluster conforme já vimos em seu algorítimo de balanceamento.

``kubectl get pods``

##### Agora vamos fazer o download dos aquivos que iremos utlizar no curso:

Execute o comando para fazer o download do repositório:

``git clone https://gitlab.com/devieiraops/manifestos-treinamento-kubernetes.git``

Navegue até a pasta pods onde se econtram os arquivos desta aula:

``cd kubernetes/pods``

Verificando o Pod em formato yaml <!--criando um manifesto yaml Acessar a pasta outros--> 

O comando gera um arquivo nginx.yaml de um pod chamado nginx com uma imagem nginx:apline
É sempre bom utlizar o comando --dry-run e realizar o export para um arquivo yaml, dessa forma você terá manifesto do seu recurso criado via comandline em um arquivo .yaml, podendo assim ser reutilizado no futuro ou para simplimentes adicionar mais configurações direto em seu manifesto.

``kubectl run --generator=run-pod/v1 nginx --image=nginx:alpine --dry-run -o yaml > nginx.yaml ``

Vamos visualizar o que foi gerado em nosso nginx.yaml

``cat nginx.yaml``

Descrevendo arquivo yaml tags básicas para executar.
- apiVersion: versão da api que cria os objetos.
- kind: define o tipo de objeto a ser criado.
- metadados: dados sobre o objeto como: nome, rótulos etc... (dicionário) (Labels > definir values)
spec: define as especificações que o objeto ulizará no container como: nome, image, ports, volumes etc... (list - array)

##### Criando um POD manifesto yaml

```
apiVersion: v1
kind: Pod
metadata:
  name: nginx
  labels:
    app: nginx
spec:
containers:
- name: nginx
  image: nginx:alpine
```

O comando cria um deployment definido em um arquivo yaml

``kubectl create -f pod-definition.yaml``

O comando mostra os pods criados

``kubectl get pods``

O comomando mostra os pods criados com mais informações como dendereço IP do pod

``kubectl get pods -o wide``

Esse comando deleta os recursos definidos no aquivo yaml. Também é possível deletar passando o nome do recurso e seu nomede de implatação.

``kubectl delete -f pod-definition.yaml`` ou ``kubectl delete pods NAMEPOD``

---        

##### OUTRAS DEFINIÇÕES PODS 
É um grupo de um ou mais containers que executam uma imagem docker.
Os pods podem utlizar recursos compatilhados como storage/network/ recursos de cpu memória.

Os pods são efemeros, ou seja, podem ser criados e recriados a qualquer instante. tanto que não devem ser tratos como durávies (se desejamos armazenar os dados precisamos Volumes)

#### Pods Lifecicle

- Pending = O Pod foi aceito pelo sistema Kubernetes, mas uma ou mais imagens container não foram criadas. Isso inclui o tempo antes de ser agendado, bem com tempo gasto no download de imagens pela rede, o que pode demorar um pouco.

-  Running = O Pod foi associado a um nó e todos os containers foram criados. A menos que um container ainda está em execução ou está em processo de iniciar.

- Succeeded = Todos os containers no pod foram encerrados com êxito e não sreiniciados.

- Failed = Todos os containers no pod foram finalizados e pelo menos um container finalizado com falha. Ou seja, o container saiu com status diferente de zero e foi encerrado pelo sistema.

- Unknown = Por alguma razão, o estado do Pod não pôde ser obtido, geralmente algum um erro na comunicação com o host do Pod.

##### Criando um POD: <!--acessa pasta Pods-->


``kubectl create -f nginx``

``kubectl get pods``

O comando descreve mais informações sobre os recursos.
Nesse exemplo sobre o pod nginx.

``kubectl describe pods nginx``
                    
##### Pods Status

- Waiting: Estado padrão do container. Se o container não estiver no estado execução ou terminado, ele estará no estado aguardando. 
Um container no estado aguardando ainda executa as operações necessárias, como extrair imagens, segredos, etc. 
Junto com esse estado, uma mensagem e o motivo do estado são exibos.

- Running: Indica que o container está sendo executado sem problemas. Esse estado também exibe a hora em que o container entrou no estado execução.

- Terminated: Indica que o container concluiu sua execução e parou de executar container passa ter este estado quando conclui com êxito a execução ou quando falha por algum motivo que deve ser analisado. 
Independentemente, um código da razão na saída do terminal é exibido.

Kubespy é uma ferramenta que demonstra em realtime o ciclo de vida dos deployment.

``kubespy trace deploy nginx`` <!--em outro terminal-->

``kubectl create -f deploynginx.yaml``

Esse comando deleta todos os recursos definidos em arquivos yaml em uma pasta. (cuidado ao utlizar essa forma, tenha certeza que queria deletar todos os recursos definidos na pasta)

``kubectl delete -f .``


### InitContainers

Os initContainers são containers que devem ser executados antes do container principal definido no POD.
Muito utlizado para realizar uma configuração antes de iniciar por completo o microservico.

Se um initContainer falhar o POD é reniciado até que ele seja efetivamente executado com sucesso.

Um exemplo seria: relizar uma permissão em uma pasta, executar um comando de nslookup ou até mesmo um git clone.

Podemos realizar configurar vários initContainers em um POD.
Dessa forma cada um é executado de forma sequencial.

Segue um exemplo de initContainer:

```
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels:
    app: myapp
spec:
  containers:
  - name: myapp-container #container principal, depois de todos os inits executados com sucesso, ele será criado.
    image: busybox:1.28
    command: ['sh', '-c', 'echo The app is running! && sleep 3600']
  initContainers:
  - name: init-myservice #primeiro sobe esse container e executa os comandos
    image: busybox:1.28
    command: ['sh', '-c', 'until nslookup myservice; do echo waiting for myservice; sleep 2; done;']
  - name: init-mydb #segundo sobe esse container e executa os comandos
    image: busybox:1.28
    command: ['sh', '-c', 'until nslookup mydb; do echo waiting for mydb; sleep 2; done;']

```

Pessoal, a cada componente do Kubernetes que estamos estudando teremos um teste de conhecimento sobre o conteúdo abordado.
Segue o link do teste:

##### Teste conhecimento pods https://forms.gle/fVWt1SgmJMbQvba68

---
