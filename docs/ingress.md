---
id: ingress
title: Ingress
---


### INGRESS

![ingress](/../img/ingress.png)
Fonte: https://vocon-it.com/2019/10/11/cka-15-kubernetes-ingress/


---


Realiza acesso externo aos serviços em um cluster.
Com Ingress podemos realizar balancemaneto de carga, terminação SSl e hospedagem virtual por nome.

O Ingress expõem rotas HTTP e HTTPS de fora do cluster para serviços dentro do cluster. 
O roteamento de tráfego é controlado pelas regras definidas no recurso do Ingress.
Podemos criar nossas regreas através do Igress baseado em conteúdo, sendo avaliado o conteúdo da solicitação e encaminha para o serviço denifido.

Roteamento do tráfego baseado no cabeçalho do host através de DNS.
Podemos utlizar annotations para reliazar configurações do servidor web.
##### Internet > Ingress > Services

Um requisito para utlizar o Ingress é ter controlador de entrada (Ingress Controller) para satisfazer uma entrada. 
Somente a criação de um recurso do Ingress não tem efeito.
                
##### Instalando Ingress-Nginx: 

Vamos acessar o path que contém os arquivos:

``cd /kubernetes/ingress-nginx``

Precisamos instalar um controlador de entreda, nesse exemplo vamos utlizar o ingress-nginx.

``cat ingress-nginx.yaml`` (instalando ingress-nginx)

``kubectl create -f ingress-nginx.yaml``

Esse comando mostra todos os ingress cridados.

``kubectl get all -n ingress-nginx``
                
##### Criando aplicações:

Vamos criar uma aplicação como exemplo para utlizar o ingress na prática.

``cat app1.yaml``

``kubectl create -f app1.yaml``

``kubectl get all``

``cat app2.yaml``

``kubectl create -f app2.yaml``

``kubectl get all``
Com isso temos dois deployments com nossas aplicaçõe de exemplo.

##### Criando Ingress:
Vamos criar o recurso de Ingress.

``cat ingress.yaml``

``kubectl create -f ingress.yaml``

Esse comando demonstra o ingress criado.

``kubectl get ing``

Vamos visualizar mais sobre o ingress criado.

``kubectl describe ing echo-ingress``

##### Vamos adicionar ao nosso arquivo hosts os DNS's em nossa máquina local:

No linux o path é:

``sudo vim /etc/hosts``

Se você utliza o Windows o path é:

``C:\Windows\System32\drivers\etc\hosts``

``127.0.0.1       echo1.hackathonshift.com.br`` (add em hosts)

``127.0.0.1       echo2.hackathonshift.com.br`` (add em hosts)

Vamos salvar e sair do arquivo hosts.

----

Executando o curl no dns passando o a rota vamos ver que somos capazes de chegar até nossa aplicação.

``curl  echo1.hackathonshift.com.br/echo1``

``curl  echo2.hackathonshift.com.br/echo2``

##### Adicionando annotations:

Vamos adidionar uma annotations no ingress e observar que nossa aplicação agora terá esse recurso.

``cat auth``

Esse comando cria uma secret, vamos detalhar mais a frente o que são secrets no Kubernetes.

``kubectl create secret generic auth --from-file=auth``

Esse comando descreve a secret que criamos.

``kubectl describe secret auth``

O comando gera uma saída da secre em formato yaml

``kubectl get secrets auth -o yaml``

``cat annotations.yaml``

``kubectl apply -f annotations.yaml``

``curl echo1.hackathonshift.com.br/echo1``

Nossa annotation é para que nossa aplicação tenha autenticação.
Executando o comando abaixo podemos ver que agora nossa aplicação solicita usuário e senha.

``curl --user "kube:kube" echo1.hackathonshift.com.br/echo1``

``curl echo2.hackathonshift.com.br/echo2``

``curl --user "kube:kube" echo2.hackathonshift.com.br/echo2``

``kubectl delete -f .``

Esse comando deleta o recuros de ingress criado.

``kubectl delete ing example-ingress``

            
##### Teste de conhecimento Ingress https://forms.gle/GyGJAeD4j4pfon986

---